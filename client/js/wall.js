function Wall (startTime, thickness, startIx, forIx, edge) {
    this.startTimeMs = startTime * 1000;
	this.inner = edge || LEVEL_EDGE_RADIUS;
    this.thickness = thickness;
    this.startIx = startIx;
    this.forIx = forIx;
    this.meshes = [];
}

Wall.TL = 0;
Wall.TR = 1;
Wall.BR = 2;
Wall.BL = 3;

Wall.prototype.toString = function() {
	return this.startIx + ':' + this.forIx + '@' + this.startTimeMs;
}
Wall.prototype.buildSegments = function(delta) {
	var segmentsLeft = this.forIx;
	var segIx = this.startIx;
	this.inner += delta;
	var outer = this.inner + this.thickness;
	while (segmentsLeft > 0) {
		var nextIx = segIx + 1;
		if (nextIx >= HEX.length) {
			nextIx -= HEX.length;
		}
		var geo = new THREE.Geometry();
		// Segment 0 is TL:0 TR:1
		//              BL:3 BR:2
		geo.vertices.push( new THREE.Vector3 ( HEX[segIx].x * outer, 1, HEX[segIx].z * outer ));
		geo.vertices.push( new THREE.Vector3 ( HEX[nextIx].x * outer, 1, HEX[nextIx].z * outer ));
		geo.vertices.push( new THREE.Vector3 ( HEX[nextIx].x * this.inner, 1, HEX[nextIx].z * this.inner ));
		geo.vertices.push( new THREE.Vector3 ( HEX[segIx].x * this.inner, 1, HEX[segIx].z * this.inner ));
		geo.faces.push( new THREE.Face3( Wall.TL, Wall.TR, Wall.BR ) );
		geo.faces.push( new THREE.Face3( Wall.TL, Wall.BR, Wall.BL ) );
		geo.computeBoundingSphere();
		geo.computeFaceNormals();
		var mat = new THREE.MeshBasicMaterial();
		mat.color = WALL_COLOR;
		this.meshes.push( new THREE.Mesh( geo, mat ) );
		segIx++;
		segmentsLeft--;
		if (segIx >= HEX.length) {
			segIx = 0;
		}
	}
};

Wall.prototype.addToScene = function(scene) {
	for (var ix = this.meshes.length - 1; ix >= 0; ix--) {
		scene.add(this.meshes[ix]);
	}
}

Wall.prototype.removeFromScene = function(scene) {
	for (var ix = this.meshes.length - 1; ix >= 0; ix--) {
		scene.remove(this.meshes[ix]);
	}
}

Wall.prototype.moveSegments = function(units) {
	this.inner -= units;
	var outer = this.inner + this.thickness;
	for (var ix = this.startIx, count = 0; count < this.forIx; ix++, count++) {
		if (ix >= HEX.length) {
			ix -= HEX.length;
		}
		var nextIx = ix + 1;
		if (nextIx >= HEX.length) {
			nextIx -= HEX.length;
		}
		var geo = this.meshes[count].geometry;
		geo.vertices[Wall.TL].x = HEX[ix].x * outer;
		geo.vertices[Wall.TL].z = HEX[ix].z * outer;
		geo.vertices[Wall.TR].x = HEX[nextIx].x * outer;
		geo.vertices[Wall.TR].z = HEX[nextIx].z * outer;
		geo.vertices[Wall.BR].x = HEX[nextIx].x * this.inner;
		geo.vertices[Wall.BR].z = HEX[nextIx].z * this.inner;
		geo.vertices[Wall.BL].x = HEX[ix].x * this.inner;
		geo.vertices[Wall.BL].z = HEX[ix].z * this.inner;
		geo.verticesNeedUpdate = true;
	}
}

Wall.prototype.testCollision = function(radius, angle, position) {
	var endIx = (this.startIx + this.forIx - 1) % HEX.length;
	var segIx = Math.floor(angle / RAD60);
	//console.log(angle + ":" + segIx);
	var segIxNext = segIx + 1;
	if (segIxNext >= HEX.length) {
		segIxNext -= HEX.length;
	}
	if (this.startIx < endIx) {
		if (segIx < this.startIx || segIx > endIx) {
			return false;
		}
	} else {
		if (segIx < this.startIx && segIx > endIx) {
			return false;
		}
	}
	// We have to test
	var segAngle = angle % RAD60;
    var lerped = HEX[segIx].clone();
	var lerpBy = 0.5 + Math.sin(segAngle - RAD30);
	lerped.lerp(HEX[segIxNext], lerpBy).multiplyScalar(this.inner);
	//console.log(lerpBy);
	//console.log(lerped);
	//console.log(position);
	//console.log(lerped.distanceToSquared(position))
	return lerped.distanceToSquared(position) < 0.1;
}

