/*global THREE, requestAnimationFrame, Wall, Stats, $ */
"use strict"; 

var superhex = { version : 1.0 };

var RAD360 = Math.PI * 2;
var RAD60 = Math.PI / 3;
var RAD30 = Math.PI / 6;
var HEX = [
    new THREE.Vector3(0, 0, 1),
    new THREE.Vector3(Math.sin(RAD60),   0, Math.cos(RAD60)),
    new THREE.Vector3(Math.sin(RAD60*2), 0, Math.cos(RAD60*2)),
    new THREE.Vector3(Math.sin(RAD60*3), 0, Math.cos(RAD60*3)),
    new THREE.Vector3(Math.sin(RAD60*4), 0, Math.cos(RAD60*4)),
    new THREE.Vector3(Math.sin(RAD60*5), 0, Math.cos(RAD60*5))];

var LEVEL_EDGE_RADIUS = 100;
var LEVEL_HEIGHT = 0;
var LEVEL_HUB_RADIUS = 6;
var LEVEL_HUB_INNER_RADIUS = 5;
var LEVEL_HUB_HEIGHT = 2;
var WALL_MOVE_PER_SEC = 20;
var PLAYER_CIRLE_SECS = 1;
var PLAYER_RADIUS = 8;
var CAMERA_DISTANCE = 100;
var CAMERA_OFFSET = 40;

var LEVEL = new Level([
	    new Wall(2.0,5,0,5),
        new Wall(4.0,5,3,5),
        new Wall(6.0,5,0,5)]);

var WALL_COLOR = new THREE.Color().setHSL(1.0,1.0,0.5);

var CursorKey = {
    _pressed: {},
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    isDown: function(keyCode) {
        return this._pressed[keyCode];
    },
    onKeydown: function(event) {
        this._pressed[event.keyCode] = true;
    },
    onKeyup: function(event) {
        delete this._pressed[event.keyCode];
    }
};

var audio = {};

superhex.go = function() {
	setupAudio();
    setupKeys();

	audio.begin.play();
    
    var stats = initStats();

    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    var cameraDist = new THREE.Vector3(CAMERA_OFFSET, CAMERA_DISTANCE, 0);
    var cameraRot = 0;
	var targetRot = 0; //Math.PI / 2;
	var target = null;
    var renderer = new THREE.WebGLRenderer();
    renderer.setClearColorHex(0xEEEEEE, 1.0);
    renderer.setSize(window.innerWidth, window.innerHeight);

    // create the ground plane
    // var planeGeometry = new THREE.PlaneGeometry(LEVEL_EDGE,LEVEL_EDGE);
    // var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff});
    // var plane = new THREE.Mesh(planeGeometry,planeMaterial);

    // plane is vertical, make it flat
    // plane.rotation.x=-0.5*Math.PI;
    // plane.position.x=0;
    // plane.position.y=-0.1;
    // plane.position.z=0;
    //scene.add(plane);
    
    scene.add( new THREE.AxisHelper( 20 ) );

	// Build the play area
    var sides = 6;
    var sliceColor1 = new THREE.Color().setHSL(1.0,1.0,0.3);
    var sliceColor2 = new THREE.Color().setHSL(1.0,1.0,0.2);
    for (var side = 0; side < sides; side++) {
        var slice = buildSlice(side, sides, LEVEL_HEIGHT, LEVEL_EDGE_RADIUS, (side & 1) ? sliceColor1 : sliceColor2 );
        scene.add(slice);
    }

	buildHub(LEVEL_HUB_RADIUS, LEVEL_HUB_INNER_RADIUS, WALL_COLOR, sliceColor2, scene);
	target = buildPlayer(WALL_COLOR, scene);

	// Add sunlight
    var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.7 );
    directionalLight.position.set( -20, 40, 60 );
    scene.add(directionalLight);

    // add subtle ambient lighting
    var ambientLight = new THREE.AmbientLight(0x101010);
    scene.add(ambientLight);

    var mR = new THREE.Matrix4();
    var mT = new THREE.Matrix4();
    mT.makeTranslation(cameraDist.x, cameraDist.y, cameraDist.z);

    // add the output of the renderer to the html element
    $("#WebGL-output").append(renderer.domElement);
    
    var gameBeginTimeMs = stats.getStartTime();
    var gameRunTimeMs = 0;
	var wallMovePerSec = WALL_MOVE_PER_SEC;
	var frameTimeMs;
    render();
	audio.music.play();
    function render() {

        stats.update();
	    var prevRunTimeMs = gameRunTimeMs;
        gameRunTimeMs = stats.getStartTime() - gameBeginTimeMs;
	    frameTimeMs = gameRunTimeMs - prevRunTimeMs;

        updateTarget(frameTimeMs);
        updateCamera();
        updateColors();
        LEVEL.addNewWalls(gameRunTimeMs, scene);
	    LEVEL.moveWalls(frameTimeMs * wallMovePerSec / 1000);
	    LEVEL.removeWalls(LEVEL_HUB_RADIUS, scene);
	    var gameOver = LEVEL.testCollision(PLAYER_RADIUS, target.rotation.y, target.position);
	    if (gameRunTimeMs < 15000  && ! gameOver) {
	        requestAnimationFrame(render);
            renderer.render(scene, camera);
	    } else {
		    audio.music.pause();
		    if (gameOver) {
			    audio.gameover.play();
		    } else {
			    audio.wonderful.play();
		    }
	    }
    }

    function updateTarget(frameTimeMs) {
        if (CursorKey.isDown(CursorKey.LEFT)) targetRot += PLAYER_CIRLE_SECS * frameTimeMs * 0.0062831853; // 2PI/1000
        if (CursorKey.isDown(CursorKey.RIGHT)) targetRot -= PLAYER_CIRLE_SECS * frameTimeMs * 0.0062831853; // 2PI/1000
        if (targetRot >= RAD360) {
            targetRot -= RAD360;
        }
        else if (targetRot < 0) {
            targetRot += RAD360;
        }
        target.rotation.y = targetRot;
        target.position.x = Math.sin(targetRot) * PLAYER_RADIUS;
        target.position.z = Math.cos(targetRot) * PLAYER_RADIUS;
    }
    
    function updateCamera() {
        cameraRot += 0.02;
        if (cameraRot >= RAD360) {
            cameraRot -= RAD360;
        }
        else if (cameraRot < 0) {
            cameraRot += RAD360;
        }
	    //cameraRot = Math.PI;
        mR.makeRotationY(cameraRot);
        mR.multiply(mT);
        camera.position = new THREE.Vector3();
        camera.position.applyMatrix4(mR);
	    camera.lookAt(new THREE.Vector3());
    }
    
    function updateColors() {
        WALL_COLOR.offsetHSL(0.01, 0, 0);
        sliceColor1.offsetHSL(0.01, 0, 0);
        sliceColor2.offsetHSL(0.01, 0, 0);
    }
    
    function initStats() {

        var stats = new Stats();

        stats.setMode(0); // 0: fps, 1: ms

        // Align top-left
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';

        $("#Stats-output").append(stats.domElement);

        return stats;
    }
    
    function setupKeys() {
        window.addEventListener('keyup', function(event) {
            CursorKey.onKeyup(event);
        }, false);
        window.addEventListener('keydown', function(event) {
            CursorKey.onKeydown(event);
        }, false);
    }

	function setupAudio() {

		audio.music = document.getElementById('audio_music');
		audio.begin = document.getElementById('audio_begin');
		audio.gameover  = document.getElementById('audio_gameover');
		audio.wonderful = document.getElementById('audio_wonderful');
	}
};

function buildPlayer(color, scene) {
	var targetGeo = new THREE.Geometry();
	targetGeo.vertices.push( new THREE.Vector3( 0, 0.1,  0) );
	targetGeo.vertices.push( new THREE.Vector3(-0.7, 0.1, -1) );
	targetGeo.vertices.push( new THREE.Vector3(+0.7, 0.1, -1) );
	targetGeo.faces.push( new THREE.Face3( 0, 2, 1 ) );
	targetGeo.computeBoundingSphere();
	targetGeo.computeFaceNormals();
	var targetMat = new THREE.MeshBasicMaterial();
	targetMat.color = color;
	var target = new THREE.Mesh(targetGeo, targetMat);
	//target.scale = new THREE.Vector3(2,2,2);
	scene.add(target);
	return target
}


function buildHub(radius, radiusInner, color, colorInner, scene) {
	var hub = [];
	for (var ix = 0; ix < HEX.length; ix++) {
		var slice      = buildSlice(ix, HEX.length, LEVEL_HUB_HEIGHT, radius, color);
		var sliceInner = buildSlice(ix, HEX.length, LEVEL_HUB_HEIGHT+0.1, radiusInner, colorInner);
		hub.push(slice);
		hub.push(sliceInner);
		if (scene) {
			scene.add(slice);
			scene.add(sliceInner);
		}
	}
	return hub;
}

function buildSlice(index, sides, height, depth, color) {
    var ix = index;
    var prevIx = index - 1;
    if (prevIx < 0) {
        ix = 0;
        prevIx = sides - 1;
    }
    var triangleGeo = new THREE.Geometry();
	//var height = new THREE.Vector3(0,height,0);
    triangleGeo.vertices.push( new THREE.Vector3() );
    triangleGeo.vertices.push( HEX[prevIx] );
    triangleGeo.vertices.push( HEX[ix] );
    triangleGeo.faces.push( new THREE.Face3( 0, 1, 2 ) );
    triangleGeo.computeBoundingSphere();
    triangleGeo.computeFaceNormals();
    var triangleMat = new THREE.MeshLambertMaterial();
    triangleMat.color = color;
    var triangle = new THREE.Mesh(triangleGeo, triangleMat);
    triangle.scale = new THREE.Vector3(depth,depth,depth);
	triangle.translateY(height);
    return triangle;
}
