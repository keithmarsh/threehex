function Level( walls ) {
	this.walls = walls;
	this.nextWallIx = 0;
}

Level.prototype.addNewWalls = function(gameTimeMs, scene) {
	// if a wall's time has come, build it
	while (this.nextWallIx < this.walls.length && gameTimeMs >= this.walls[this.nextWallIx].startTimeMs ) {
		// build wall segments
		this.walls[this.nextWallIx].buildSegments(0);
		if (scene) {
			this.walls[this.nextWallIx].addToScene(scene);
		}
		this.nextWallIx++;
	}
};

Level.prototype.moveWalls = function(units) {
	for (var ix = 0; ix < this.walls.length; ix++) {
		if (this.walls[ix] !== undefined &&
			this.walls[ix].meshes !== null &&
			this.walls[ix].meshes.length > 0)
		{
			this.walls[ix].moveSegments(units);
		}
	}
}

Level.prototype.removeWalls = function(stopAt, scene) {
	for (var ix = 0; ix < this.walls.length; ix++) {
		if (this.walls[ix] !== undefined && (this.walls[ix].inner + this.walls[ix].thickness) <= stopAt && this.walls[ix].meshes != null) {
			if (scene) {
				this.walls[ix].removeFromScene(scene);
			}
			this.walls[ix].meshes = null;
		}
	}

}

Level.prototype.testCollision = function(radius, angle, position) {

	// Find the candidate wall by rewinding from nextWall
	for (var ix = this.nextWallIx; ix >= 0; ix--) {
		if (ix == this.walls.length) {
			continue;
		}
		if (this.walls[ix] && this.walls[ix].meshes == null) {
			// reached old deleted walls
			break;
		}
		if (this.walls[ix].inner < radius) {
			// reached walls past target
			break;
		}
		// Left Hand Vertical Wall
		if (this.walls[ix].testCollision(radius, angle, position)) {
			return true;
		}
	}
	return false;
}

// if a wall has reached the hub, kill it
