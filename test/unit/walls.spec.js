/*global describe, it, expect, spyOn, toHaveBeenCalled, toEqual, beforeEach */
describe('building walls', function() {
	var level;
	var scene;
	beforeEach(function() {
		var wall1 = new Wall(10.0, 1.0, 0, 3, 10);
		var wall2 = new Wall(15.0, 1.0, 3, 3, 10);
		level = new Level( [ wall1, wall2 ] );
		scene = { add : function(x) { } };
	});
	it('should call build wall segments when its time to create', function() {
		spyOn(level.walls[0], 'buildSegments');
		spyOn(level.walls[1], 'buildSegments');
		spyOn(level.walls[0], 'addToScene');
		spyOn(level.walls[1], 'addToScene');
		expect(level.walls[0].buildSegments).not.toHaveBeenCalled();
		expect(level.walls[1].buildSegments).not.toHaveBeenCalled();
		level.addNewWalls(10000, scene);
		expect(level.nextWallIx).toEqual(1);
		expect(level.walls[0].addToScene.calls.length).toEqual(1);
		expect(level.walls[1].addToScene.calls.length).toEqual(0);
		level.addNewWalls(20000, scene);
		expect(level.nextWallIx).toEqual(2);
		expect(level.walls[0].addToScene.calls.length).toEqual(1);
		expect(level.walls[1].addToScene.calls.length).toEqual(1);
	});
	it('should add walls to the scene', function() {
		spyOn(scene, 'add');
		level.addNewWalls(0, scene);
		expect(scene.add).not.toHaveBeenCalled();
		level.addNewWalls(10000, scene);
		expect(scene.add.calls.length).toEqual(3);
		level.addNewWalls(20000, scene);
		expect(scene.add.calls.length).toEqual(6);
	})
	it('should build the walls in the right place', function() {
		level.addNewWalls(20000, scene);
		expect(level.walls[0].meshes.length).toEqual(3);
		expect(level.walls[0].meshes[0].geometry.vertices[Wall.BL].x).toEqual(0);
		expect(level.walls[0].meshes[0].geometry.vertices[Wall.BL].z).toEqual(10);
		expect(level.walls[0].meshes[2].geometry.vertices[Wall.BR].x).toBeCloseTo(0);
		expect(level.walls[0].meshes[2].geometry.vertices[Wall.BR].z).toEqual(-10);


		expect(level.walls[1].meshes[0].geometry.vertices[Wall.BL].x).toBeCloseTo(0);
		expect(level.walls[1].meshes[0].geometry.vertices[Wall.BL].z).toEqual(-10);
	});
});
describe('moving walls', function() {
	var level;
	var scene;
	beforeEach(function() {
		var wall1 = new Wall(0.0, 1.0, 0, 1, 3);
		var wall2 = new Wall(0.0, 1.0, 3, 1, 3);
		level = new Level([ wall1, wall2 ]);
		scene = { add : function(x) { } };
	});
	it('should move a wall in by the steps per ms', function() {
		level.addNewWalls(0, scene);
		expect(level.walls[0].meshes[0].geometry.vertices[Wall.BL].x).toBeCloseTo(0);
		expect(level.walls[0].meshes[0].geometry.vertices[Wall.BL].z).toEqual(3);
		expect(level.walls[1].meshes[0].geometry.vertices[Wall.BL].x).toBeCloseTo(0);
		expect(level.walls[1].meshes[0].geometry.vertices[Wall.BL].z).toEqual(-3);
		level.moveWalls(1);
		expect(level.walls[0].meshes[0].geometry.vertices[Wall.BL].x).toBeCloseTo(0);
		expect(level.walls[0].meshes[0].geometry.vertices[Wall.BL].z).toEqual(2);
		expect(level.walls[1].meshes[0].geometry.vertices[Wall.BL].x).toBeCloseTo(0);
		expect(level.walls[1].meshes[0].geometry.vertices[Wall.BL].z).toEqual(-2);
	});
	it('should only move the walls in', function() {

	});
});

describe('removing walls', function() {
	it('move a wall past stopAt deletes it', function() {
		var wall1 = new Wall(0.0, 1.0, 0, 1, 3);
		var level = new Level([ wall1 ]);
		var scene = { add: function(x) { }, remove : function(x) { } };
		spyOn(scene, 'remove');
		level.addNewWalls(0, scene);
		expect(wall1.meshes[0].geometry.vertices[Wall.BL].x).toEqual(0);
		expect(wall1.meshes[0].geometry.vertices[Wall.BL].z).toEqual(3);
		level.removeWalls(3, scene);
		expect(scene.remove).not.toHaveBeenCalled();
		expect(level.walls[0].meshes).not.toBeNull();
		level.removeWalls(4, scene);
		expect(scene.remove).toHaveBeenCalled();
		expect(level.walls[0].meshes).toBeNull();
	});
});
describe('colliding wall', function() {
	it('will detect a collision', function() {
		var wall1 = new Wall(0.0, 1.0, 0, 1, 3);
		var level = new Level([ wall1 ]);
		var scene = { add: function(x) { }, remove : function(x) { } };
		level.addNewWalls(0, scene);
		expect(wall1.meshes[0].geometry.vertices[Wall.BL].x).toEqual(0);
		expect(wall1.meshes[0].geometry.vertices[Wall.BL].z).toEqual(3);
		var collided = level.testCollision(3, 0, new THREE.Vector3(0,0,3));
		expect(collided).toBeTruthy();
	});
	it('will not detect a collision when none exist', function() {
		var wall1 = new Wall(0.0, 1.0, 0, 1, 3);
		var level = new Level([ wall1 ]);
		var scene = { add: function(x) { }, remove : function(x) { } };
		level.addNewWalls(0, scene);
		expect(wall1.meshes[0].geometry.vertices[Wall.BL].x).toEqual(0);
		expect(wall1.meshes[0].geometry.vertices[Wall.BL].z).toEqual(3);
		var collided = level.testCollision(2, 0, new THREE.Vector3(0,0,2));
		expect(collided).toBeFalsy();

	});
});