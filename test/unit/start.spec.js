/*global describe, it, expect, spyOn, toHaveBeenCalled, toEqual, beforeEach*/
describe('drawing a wall', function() {
    var wall;
    beforeEach(function() {
        wall = new Wall(0, 10, 4, 4);
	    wall.buildSegments(0);
	    //buildWall(1, 6, 200, 10, 0x332211);
        //wall = buildSlice(1,2,3,0x112233);
    });
    it('should have a Mesh segment', function() {
	    expect(wall.meshes).toBeDefined();
	    expect(Array.isArray(wall.meshes)).toBeTruthy();
        expect(wall.meshes[0] instanceof THREE.Mesh).toBeTruthy();
    });
    it('should have four vertices per segment', function() {
        expect(wall.meshes[0].geometry).toBeDefined();
        expect(wall.meshes[0].geometry.vertices).toBeDefined();
        expect(wall.meshes[0].geometry.vertices.length).toBeDefined();
        expect(wall.meshes[0].geometry.vertices.length).toEqual(4);
    });
    it('should have two faces per segment', function() {
        expect(wall.meshes[0].geometry.faces).toBeDefined();
        expect(wall.meshes[0].geometry.faces.length).toBeDefined();
        expect(wall.meshes[0].geometry.faces.length).toEqual(2);
    });
	it('should have four segments', function() {
		expect(wall.meshes.length).toEqual(4);
	})
});

describe('adding a wall', function() {
    // var wall1, wall2;
    // it('will accept a start time, duration, start index and index count', function() {
    //     wall1 = addWall(10, 1,1,10);
    //     expect(wallCount).toBe(1);
        
    // });
    // it('will accept multiple walls', function() {
    //     wall2 = addWall(2,2,10);
    //     expect(wallCount).toBe(2);
    // });
    // it('will move toward the center linearly', function() {
    //     moveWalls(0.1);        
    // });
    // it('will be destroyed when it reaches the centre', function() {
    //     moveWalls(1000);
    //     expect(wallCount).toBe(0);
    // });
    it('will have an easy API to design levels', function() {
        var walls = [
            new Wall(0.0,0.1,0,5),
            new Wall(0.4,0.1,3,5),
            new Wall(0.8,0.1,0,5)]; 
    });
});