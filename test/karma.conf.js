module.exports = function(config) {
    config.set({
        basePath: '..',
        frameworks: ['jasmine'],
        singleRun: true,
        browsers : ['PhantomJS'],
        hostname : process.env.IP || "127.0.0.1",
        port : process.env.PORT || 9876,
        runnerPort : 0,
        files : [
            'client/js/three.js',
	        'client/js/level.js',
	        'client/js/wall.js',
            'client/js/superhex.js',
            'test/unit/*.spec.js'
            ],
	    logLevel: LOG_INFO
  });
};